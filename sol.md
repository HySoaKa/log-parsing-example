# Executing the script


## Windows
```
type log.tsv | python parse-log.py
```

## Linux
```
cat log.tsv | python parse-log.py
```
