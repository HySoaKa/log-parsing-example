from pyparsing import Suppress, Word, alphas, nums
import sys


slash = Suppress('/')

# creates parser object
url = slash + Word(alphas) \
    + slash + Word(nums + ".") \
    + slash + Word(alphas + "_-") \
    + slash + Word(alphas + "_-")('viewmode') \
    + slash + Word(nums) \
    + slash + Word(nums)('zoom') \
    + slash + Word(nums) \
    + slash + Word(nums)

results = list()
# looks through input text and generates matches
for match in url.scanString(sys.stdin.readlines()):
    result = match[0]
    # adds first result if results list is empty
    if not results:
        results.append({
            'viewmode': result.viewmode,
            'nmbr_sqtial_occ': 1,
            'zoom': set([result.zoom])
            })
    # else compares viewmode of current result with the prvious one:
    # if viewmode is the same, adds one to the number of sequential occurence
    # and adds zoom
    elif results[-1]['viewmode'] == result.viewmode:
        results[-1]['nmbr_sqtial_occ'] = results[-1]['nmbr_sqtial_occ'] + 1
        results[-1]['zoom'].add(result.zoom)
    # else adds result
    else:
        results.append({
            'viewmode': result.viewmode,
            'nmbr_sqtial_occ': 1,
            'zoom': set([result.zoom])
            })

# prints results
for result in results:
    print("{}\t{}\t{}".format(
        result['viewmode'],
        result['nmbr_sqtial_occ'],
        ", ".join(result['zoom']))
        )
